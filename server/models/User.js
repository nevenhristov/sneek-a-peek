var mongoose = require('mongoose');

//Users schema
var usersSchema = mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    }
});

var User = module.exports = mongoose.model('User', usersSchema);

module.exports.getUsers = function (cb) {
    User.find(cb);
};

module.exports.addUser = function (user, cb) {
    User.create(user, cb);
};