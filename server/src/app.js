const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');

let index = require('../routes/index');
let users = require('../routes/users');
let port = process.env.PORT || 8081;

const app = express();

app.use(express.static(__dirname + '/client'));

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());

// Connect to database
mongoose.connect(`mongodb://admin:admin1234@ds227171.mlab.com:27171/registered-users`);

//check connection
var db = mongoose.connection;
db.on("error", console.error.bind(console, "Database connection error"));
db.once("open", function (callback) {
    console.log("Database connection successful");
});

app.use('/', index);
app.use('/', users);


app.listen(port)
console.log('server listening on port: ', port);

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});