'use strict';

var express = require('express');
var router = express.Router();
var User = require('../models/User');

//get all users from the DB
router.get('/users', function (req, res) {
    User.getUsers(function (err, users) {
        if (err) {
            res.status(404).send('No  users Found');
        }
        //return all users
        res.json(users);
    });
});

module.exports = router;