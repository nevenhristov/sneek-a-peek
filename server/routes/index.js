'use strict';

var express = require('express');
var router = express.Router();
var User = require('../models/User');

//add a new user in the DB
router.post('/add', function (req, res) {
    var user = req.body;

    User.addUser(user, function (err, user) {
        if (err) {
            res.status(400).send('Unable to create user');
        }
        res.json(user);
    });
});

module.exports = router;