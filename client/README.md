# Sneek-a-peek Front-end

> Simple VueJS register form, saving user data into a mongoDB. Using express server and NodeJS. (MEVN Stack)

## Build Setup

Navigate to the /client directory of the project on the node cmd, and run the command to install all the dependencies.
Do the same but at the /server directory to install all the express server dependencies.
``` bash
# install dependencies
npm install
```


After the dependencies are installed, at the /client directory use the command npm run dev and will start the development server for the front-end.
``` bash
# serve with hot reload at localhost:8080
npm run dev
```


Navigate to the /server directory in a new instance of node cmd and run the command npm start to start the dev express server.
``` bash
# starts the express server with nodemon for auto restaring on change 
npm start
```


Build assets for production. All static assets compiled with version hashes for efficient long-term caching, 
and a production index.html is auto-generated with proper URLs to these generated assets.
``` bash
# build for production with minification
npm run build
```


``` bash
# build for production and view the bundle analyzer report
npm run build --report
```


``` bash
# run unit tests
npm run unit
```


``` bash
# run all tests
npm test
```
## Database

The mongoDB is hosted online at [mLab](https://mlab.com/).


## Deploy

After running the **npm run build** command in the client folder there will be a **dist** folder created.
The **dist** folder contains the **assets** folder which contains all your compiled static assets (JS and CSS).
It also contains the **index.html**, his is the entry point of the application. It’s a minified HTML document with links to the apps CSS and JS.
Copy this folder into the root folder of your web server such as [AWS](https://aws.amazon.com/) 
