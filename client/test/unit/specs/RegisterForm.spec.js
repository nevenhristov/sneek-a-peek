import Vuex from 'vuex';
import {
  shallow,
  createLocalVue
} from 'vue-test-utils';
import VeeValidate from "vee-validate";
import RegisterForm from '@/components/RegisterForm';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VeeValidate);

describe('RegisterForm.vue', () => {
  let component = {};
  let state = {
    user: {
      username: '',
      password: '',
      email: ''
    },
    users: []
  };

  const store = new Vuex.Store({
    state
  });

  beforeEach(() => {
    component = shallow(RegisterForm, {
      localVue,
      store
    });
  });

  it('is a Vue instance', () => {
    expect(component.isVueInstance()).toBeTruthy();
  });
});
