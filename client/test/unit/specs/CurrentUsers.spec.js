import Vuex from 'vuex';
import {
  shallow,
  createLocalVue
} from 'vue-test-utils';
import CurrentUsers from '@/components/CurrentUsers';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('CurrentUsers.Vue', () => {
  let component = {};
  let state = {
    user: {
      username: '',
      password: '',
      email: ''
    },
    users: []
  };

  let mutations = {
    CURRENT_USERS: jest.fn()
  };

  let actions = {
    getAllUsers: jest.fn()
  };

  const store = new Vuex.Store({
    state,
    mutations,
    actions
  });

  beforeEach(() => {
    component = shallow(CurrentUsers, {
      localVue,
      store
    });
  });
  
  it('is a Vue instance', () => {
    expect(component.isVueInstance()).toBeTruthy();
  });

  it('dispatches a getAllUsers action on button click', () => {
    component.find('button').trigger('click');
    expect(actions.getAllUsers.mock.calls).toHaveLength(1)
  });

  it('commits a CURRENT_USERS mutation when a button is clicked', () => {
    component.find('button').trigger('click');

    store.dispatch('getAllUsers')
      .then(() => {
        try {
          expect(mutations.CURRENT_USERS.mock.calls).toHaveLength(1);
        } catch (err) {}
      })
  });
})
