import Api from '@/services/Api';

export default {
  fetchUsers () {
    return Api().get('users')
  },
  postUser (user) {
      return Api().post('add', user)
  }
}