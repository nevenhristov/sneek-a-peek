import Vue from "vue";
import Vuex from "vuex";
import UsersService from "@/services/UsersService";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {
      username: "",
      password: "",
      email: ""
    },
    users: []
  },
  mutations: {
    CURRENT_USERS: (state, data) => {
      state.users = data;
    },
    RESET_USER: (state) => {
      state.user.username = '';
      state.user.password = '';
      state.user.email = '';
    }
  },
  actions: {
    async getAllUsers({
      commit
    }) {
      try {
        const response = await UsersService.fetchUsers();
        commit('CURRENT_USERS', response.data);
        if (response.data.length < 1) {
          alert('No users registered yet.'); //placeholder
        }
      } catch (error) {
        console.error(error);
      }
    }
  }
});
