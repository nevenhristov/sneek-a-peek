import Vue from 'vue';
import Router from 'vue-router';
import RegisterForm from '@/components/RegisterForm';
import CurrentUsers from '@/components/CurrentUsers';

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'RegisterForm',
      component: RegisterForm
    },
    {
      path: '/users',
      name: 'CurrentUsers',
      component: CurrentUsers
    }
  ]
})
